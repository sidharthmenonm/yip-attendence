import Vue from 'vue'
import WebCam from "vue-web-cam";
import App from "./app.vue";
Vue.use(WebCam);

new Vue({
  el: "#app",
  render: h => h(App)
});